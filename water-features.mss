#piers-poly, #piers-line {
  [man_made = 'pier'][zoom >= 12] {
    #piers-poly {
      polygon-fill: @land-color;
    }
    #piers-line {
      line-width: 0.5;
      line-color: @land-color;
      line-cap: square;
      [zoom >= 13] { line-width: 1; }
      [zoom >= 15] { line-width: 2; }
      [zoom >= 17] { line-width: 4; }
    }
  }

  [man_made = 'breakwater'][zoom >= 12],
  [man_made = 'groyne'][zoom >= 12] {
    #piers-poly {
      polygon-fill: @land-color;
    }
    #piers-line {
      line-width: 1;
      line-color: @land-color;
      [zoom >= 13] { line-width: 2; }
      [zoom >= 16] { line-width: 4; }
    }
  }
}

#text-line {
  [feature = 'waterway_dam'],
  [feature = 'waterway_weir'] {
    #text-line[zoom >= 15] {
      text-name: "[name]";
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-fill: @text-color;
      text-size: 10;
      text-face-name: @book-fonts;
      #text-poly {
        text-placement: interior;
      }
      #text-line {
        text-placement: line;
        text-dy: 8;
        text-spacing: 400;
      }
    }
  }

  [feature = 'man_made_breakwater'][zoom >= 15],
  [feature = 'man_made_groyne'][zoom >= 15],
  [feature = 'man_made_pier'][zoom >= 15] {
    #text-line {
      text-name: "[name]";
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @standard-halo-fill;
      text-fill: @text-color;
      text-size: 10;
      text-face-name: @book-fonts;
      #text-poly {
        text-placement: interior;
      }
      #text-line {
        text-placement: line;
        text-spacing: 400;
      }
    }
  }
}
