/* For the main linear features, such as roads and railways. */

@roads-text-color: @text-color;
@roads-halo-fill: rgba(0,0,0,0.6);

@motorway-width-z6:               0.4;
@trunk-width-z6:                  0.4;

@motorway-width-z7:               0.8;
@trunk-width-z7:                  0.6;

@motorway-width-z8:               1;
@trunk-width-z8:                  1;
@primary-width-z8:                1;

@motorway-width-z9:               1.4;
@trunk-width-z9:                  1.4;
@primary-width-z9:                1.4;
@secondary-width-z9:              1;

@motorway-width-z10:              1.9;
@trunk-width-z10:                 1.9;
@primary-width-z10:               1.8;
@secondary-width-z10:             1.1;
@tertiary-width-z10:              0.7;

@motorway-width-z11:              2.0;
@trunk-width-z11:                 1.9;
@primary-width-z11:               1.8;
@secondary-width-z11:             1.1;
@tertiary-width-z11:              0.7;

@motorway-width-z12:              3.5;
@motorway-link-width-z12:         1.5;
@trunk-width-z12:                 3.5;
@trunk-link-width-z12:            1.5;
@primary-width-z12:               3.5;
@primary-link-width-z12:          1.5;
@secondary-width-z12:             3.5;
@secondary-link-width-z12:        1.5;
@tertiary-width-z12:              2.5;
@tertiary-link-width-z12:         1.5;
@residential-width-z12:           0.5;
@unclassified-width-z12:          0.8;

@motorway-width-z13:              6;
@motorway-link-width-z13:         4;
@trunk-width-z13:                 6;
@trunk-link-width-z13:             4;
@primary-width-z13:               5;
@primary-link-width-z13:          4;
@secondary-width-z13:             5;
@secondary-link-width-z13:        4;
@tertiary-width-z13:              4;
@tertiary-link-width-z13:         3;
@residential-width-z13:           2.5;
@living-street-width-z13:         2;
@bridleway-width-z13:             0.3;
@footway-width-z14:               0.7;
@cycleway-width-z13:              0.7;
@track-width-z13:                 0.5;
@track-grade1-width-z13:          0.5;
@track-grade2-width-z13:          0.5;
@steps-width-z13:                 0.7;

@secondary-width-z14:             5;
@tertiary-width-z14:              5;
@residential-width-z14:           3;
@living-street-width-z14:         3;
@pedestrian-width-z14:            3;
@road-width-z14:                  2;
@service-width-z14:               2;

@motorway-width-z15:             10;
@motorway-link-width-z15:         7.8;
@trunk-width-z15:                10;
@trunk-link-width-z15:          7.8;
@primary-width-z15:              10;
@primary-link-width-z15:        7.8;
@secondary-width-z15:             9;
@secondary-link-width-z15:        7;
@tertiary-width-z15:              9;
@tertiary-link-width-z15:         7;
@residential-width-z15:           5;
@living-street-width-z15:         5;
@pedestrian-width-z15:            5;
@bridleway-width-z15:             1.2;
@footway-width-z15:               1;
@cycleway-width-z15:              0.9;
@track-width-z15:                 1.5;
@track-grade1-width-z15:          0.75;
@track-grade2-width-z15:          0.75;
@steps-width-z15:                 3;

@secondary-width-z16:            10;
@tertiary-width-z16:             10;
@residential-width-z16:           6;
@living-street-width-z16:         6;
@pedestrian-width-z16:            6;
@road-width-z16:                  3.5;
@service-width-z16:               3.5;
@minor-service-width-z16:         2;
@footway-width-z16:               1.3;
@cycleway-width-z16:              0.9;

@motorway-width-z17:             18;
@motorway-link-width-z17:        12;
@trunk-width-z17:                18;
@trunk-link-width-z17:           12;
@primary-width-z17:              18;
@primary-link-width-z17:         12;
@secondary-width-z17:            18;
@secondary-link-width-z17:       12;
@tertiary-width-z17:             18;
@tertiary-link-width-z17:        12;
@residential-width-z17:          12;
@living-street-width-z17:        12;
@pedestrian-width-z17:           12;
@road-width-z17:                  7;
@service-width-z17:               7;
@minor-service-width-z17:         3.5;

@motorway-width-z18:             21;
@motorway-link-width-z18:        13;
@trunk-width-z18:                21;
@trunk-link-width-z18:           13;
@primary-width-z18:              21;
@primary-link-width-z18:         13;
@secondary-width-z18:            21;
@secondary-link-width-z18:       13;
@tertiary-width-z18:             21;
@tertiary-link-width-z18:        13;
@residential-width-z18:          13;
@living-street-width-z18:        13;
@pedestrian-width-z18:           13;
@road-width-z18:                  8.5;
@service-width-z18:               8.5;
@minor-service-width-z18:         4.75;
@footway-width-z18:               1.3;
@cycleway-width-z18:              1;

@motorway-width-z19:             27;
@motorway-link-width-z19:        16;
@trunk-width-z19:                27;
@trunk-link-width-z19:           16;
@primary-width-z19:              27;
@primary-link-width-z19:         16;
@secondary-width-z19:            27;
@secondary-link-width-z19:       16;
@tertiary-width-z19:             27;
@tertiary-link-width-z19:        16;
@residential-width-z19:          17;
@living-street-width-z19:        17;
@pedestrian-width-z19:           17;
@road-width-z19:                 11;
@service-width-z19:              11;
@minor-service-width-z19:         5.5;
@footway-width-z19:               1.6;
@cycleway-width-z19:              1.3;

@motorway-width-z20:             33;
@motorway-link-width-z20:        17;
@service-width-z20:              12;
@minor-service-width-z20:         8.5;


@casing-multiplier:               4;

@major-casing-width-z11:          0.3*@casing-multiplier;

@casing-width-z12:                0.3*@casing-multiplier;
@secondary-casing-width-z12:      0.3*@casing-multiplier;
@major-casing-width-z12:          0.5*@casing-multiplier;

@casing-width-z13:                0.5*@casing-multiplier;
@residential-casing-width-z13:    0.5*@casing-multiplier;
@secondary-casing-width-z13:      0.35*@casing-multiplier;
@major-casing-width-z13:          0.5*@casing-multiplier;

@casing-width-z14:                0.55*@casing-multiplier;
@secondary-casing-width-z14:      0.35*@casing-multiplier;
@major-casing-width-z14:          0.6*@casing-multiplier;

@casing-width-z15:                0.6*@casing-multiplier;
@secondary-casing-width-z15:      0.7*@casing-multiplier;
@major-casing-width-z15:          0.7*@casing-multiplier;

@casing-width-z16:                0.6*@casing-multiplier;
@secondary-casing-width-z16:      0.7*@casing-multiplier;
@major-casing-width-z16:          0.7*@casing-multiplier;

@casing-width-z17:                0.8*@casing-multiplier;
@secondary-casing-width-z17:      1*@casing-multiplier;
@major-casing-width-z17:          1*@casing-multiplier;

@casing-width-z18:                0.8*@casing-multiplier;
@secondary-casing-width-z18:      1*@casing-multiplier;
@major-casing-width-z18:          1*@casing-multiplier;

@casing-width-z19:                0.8*@casing-multiplier;
@secondary-casing-width-z19:      1*@casing-multiplier;
@major-casing-width-z19:          1*@casing-multiplier;

@casing-width-z20:                0.8*@casing-multiplier;
@secondary-casing-width-z20:      1*@casing-multiplier;
@major-casing-width-z20:          1*@casing-multiplier;

@bridge-casing-width-z12:         0.1*@casing-multiplier;
@major-bridge-casing-width-z12:   0.5*@casing-multiplier;
@bridge-casing-width-z13:         0.5*@casing-multiplier;
@major-bridge-casing-width-z13:   0.5*@casing-multiplier;
@bridge-casing-width-z14:         0.5*@casing-multiplier;
@major-bridge-casing-width-z14:   0.6*@casing-multiplier;
@bridge-casing-width-z15:         0.75*@casing-multiplier;
@major-bridge-casing-width-z15:   0.75*@casing-multiplier;
@bridge-casing-width-z16:         0.75*@casing-multiplier;
@major-bridge-casing-width-z16:   0.75*@casing-multiplier;
@bridge-casing-width-z17:         0.8*@casing-multiplier;
@major-bridge-casing-width-z17:   1*@casing-multiplier;
@bridge-casing-width-z18:         0.8*@casing-multiplier;
@major-bridge-casing-width-z18:   1*@casing-multiplier;
@bridge-casing-width-z19:         0.8*@casing-multiplier;
@major-bridge-casing-width-z19:   1*@casing-multiplier;
@bridge-casing-width-z20:         0.8*@casing-multiplier;
@major-bridge-casing-width-z20:   1*@casing-multiplier;

@paths-background-width:          1*@casing-multiplier;
@paths-bridge-casing-width:       0.5*@casing-multiplier;
@paths-tunnel-casing-width:       1*@casing-multiplier;

@lowzoom-halo-width:              1;

// Shield’s line wrap is based on OpenStreetMap data and not on line-wrap-width,
// but lines are typically rather short, so we use narrow line spacing.
@shield-size: 10;
@shield-line-spacing: -1.50; // -0.15 em
@shield-size-z16: 11;
@shield-line-spacing-z16: -1.65; // -0.15 em
@shield-size-z18: 12;
@shield-line-spacing-z18: -1.80; // -0.15 em
@shield-spacing: 760;
@shield-repeat-distance: 400;
@shield-margin: 40;
@shield-font: @book-fonts;
@shield-clip: false;

@major-highway-text-repeat-distance: 50;
@minor-highway-text-repeat-distance: 10;

@railway-text-repeat-distance: 200;

/* Data on z<10 comes from osm_planet_roads, data on z>=10 comes from
osm_planet_line. This is for performance reasons: osm_planet_roads contains less
data, and is thus faster. Chosen is for zoom level 10 as cut-off, because
tertiary is rendered from z10 and is not included in osm_planet_roads. */

#roads-low-zoom[zoom < 10],
#roads-fill[zoom >= 10],
#bridges[zoom >= 10],
#tunnels[zoom >= 10] {

  ::casing {
    line-color: @roads-casing-color;

    [zoom >= 12] {
      [feature = 'highway_motorway'] {
        line-width: @motorway-width-z12;
        [zoom >= 13] { line-width: @motorway-width-z13; }
        [zoom >= 15] { line-width: @motorway-width-z15; }
        [zoom >= 17] { line-width: @motorway-width-z17; }
        [zoom >= 18] { line-width: @motorway-width-z18; }
        [zoom >= 19] { line-width: @motorway-width-z19; }
        [zoom >= 20] { line-width: @motorway-width-z20; }
        [link = 'yes'] {
          line-width: @motorway-link-width-z12;
          [zoom >= 13] { line-width: @motorway-link-width-z13; }
          [zoom >= 15] { line-width: @motorway-link-width-z15; }
          [zoom >= 17] { line-width: @motorway-link-width-z17; }
          [zoom >= 18] { line-width: @motorway-link-width-z18; }
          [zoom >= 19] { line-width: @motorway-link-width-z19; }
          [zoom >= 20] { line-width: @motorway-link-width-z20; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_trunk'] {
      [zoom >= 12] {
        line-width: @trunk-width-z12;
        [zoom >= 13] { line-width: @trunk-width-z13; }
        [zoom >= 15] { line-width: @trunk-width-z15; }
        [zoom >= 17] { line-width: @trunk-width-z17; }
        [zoom >= 18] { line-width: @trunk-width-z18; }
        [zoom >= 19] { line-width: @trunk-width-z19; }
        [link = 'yes'] {
          line-width: @trunk-link-width-z12;
          [zoom >= 13] { line-width: @trunk-link-width-z13; }
          [zoom >= 15] { line-width: @trunk-link-width-z15; }
          [zoom >= 17] { line-width: @trunk-link-width-z17; }
          [zoom >= 18] { line-width: @trunk-link-width-z18; }
          [zoom >= 19] { line-width: @trunk-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_primary'] {
      [zoom >= 12] {
        line-width: @primary-width-z12;
        [zoom >= 13] { line-width: @primary-width-z13; }
        [zoom >= 15] { line-width: @primary-width-z15; }
        [zoom >= 17] { line-width: @primary-width-z17; }
        [zoom >= 18] { line-width: @primary-width-z18; }
        [zoom >= 19] { line-width: @primary-width-z19; }
        [link = 'yes'] {
          line-width: @primary-link-width-z12;
          [zoom >= 13] { line-width: @primary-link-width-z13; }
          [zoom >= 15] { line-width: @primary-link-width-z15; }
          [zoom >= 17] { line-width: @primary-link-width-z17; }
          [zoom >= 18] { line-width: @primary-link-width-z18; }
          [zoom >= 19] { line-width: @primary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_secondary'] {
      [zoom >= 12] {
        line-width: @secondary-width-z12;
        [zoom >= 13] { line-width: @secondary-width-z13; }
        [zoom >= 14] { line-width: @secondary-width-z14; }
        [zoom >= 15] { line-width: @secondary-width-z15; }
        [zoom >= 16] { line-width: @secondary-width-z16; }
        [zoom >= 17] { line-width: @secondary-width-z17; }
        [zoom >= 18] { line-width: @secondary-width-z18; }
        [zoom >= 19] { line-width: @secondary-width-z19; }
        [link = 'yes'] {
          line-width: @secondary-link-width-z12;
          [zoom >= 13] { line-width: @secondary-link-width-z13; }
          [zoom >= 15] { line-width: @secondary-link-width-z15; }
          [zoom >= 17] { line-width: @secondary-link-width-z17; }
          [zoom >= 18] { line-width: @secondary-link-width-z18; }
          [zoom >= 19] { line-width: @secondary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 13] {
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_tertiary'] {
      [zoom >= 12] {
        line-width: @tertiary-width-z12;
        [zoom >= 13] { line-width: @tertiary-width-z13; }
        [zoom >= 14] { line-width: @tertiary-width-z14; }
        [zoom >= 15] { line-width: @tertiary-width-z15; }
        [zoom >= 16] { line-width: @tertiary-width-z16; }
        [zoom >= 17] { line-width: @tertiary-width-z17; }
        [zoom >= 18] { line-width: @tertiary-width-z18; }
        [zoom >= 19] { line-width: @tertiary-width-z19; }
        [link = 'yes'] {
          line-width: @tertiary-link-width-z12;
          [zoom >= 13] { line-width: @tertiary-link-width-z13; }
          [zoom >= 15] { line-width: @tertiary-link-width-z15; }
          [zoom >= 17] { line-width: @tertiary-link-width-z17; }
          [zoom >= 18] { line-width: @tertiary-link-width-z18; }
          [zoom >= 19] { line-width: @tertiary-link-width-z19; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 14] {
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_residential'],
    [feature = 'highway_unclassified'] {
      [zoom >= 13] {
        line-width: @residential-width-z13;
        [zoom >= 14] { line-width: @residential-width-z14; }
        [zoom >= 15] { line-width: @residential-width-z15; }
        [zoom >= 16] { line-width: @residential-width-z16; }
        [zoom >= 17] { line-width: @residential-width-z17; }
        [zoom >= 18] { line-width: @residential-width-z18; }
        [zoom >= 19] { line-width: @residential-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 14] {
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_road'] {
      [zoom >= 14] {
        line-width: @road-width-z14;
        [zoom >= 16] { line-width: @road-width-z16; }
        [zoom >= 17] { line-width: @road-width-z17; }
        [zoom >= 18] { line-width: @road-width-z18; }
        [zoom >= 19] { line-width: @road-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_service'] {
      [zoom >= 14][service = 'INT-normal'],
      [zoom >= 16][service = 'INT-minor'] {
        [service = 'INT-normal'] {
          line-width: @service-width-z14;
          [zoom >= 16] { line-width: @service-width-z16; }
          [zoom >= 17] { line-width: @service-width-z17; }
          [zoom >= 18] { line-width: @service-width-z18; }
          [zoom >= 19] { line-width: @service-width-z19; }
          [zoom >= 20] { line-width: @service-width-z20; }
        }
        [service = 'INT-minor'] {
          line-width: @minor-service-width-z16;
          [zoom >= 17] { line-width: @minor-service-width-z17; }
          [zoom >= 18] { line-width: @minor-service-width-z18; }
          [zoom >= 19] { line-width: @minor-service-width-z19; }
          [zoom >= 20] { line-width: @minor-service-width-z20; }
        }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_pedestrian'] {
      [zoom >= 14] {
        line-width: @pedestrian-width-z14;
        [zoom >= 15] { line-width: @pedestrian-width-z15; }
        [zoom >= 16] { line-width: @pedestrian-width-z16; }
        [zoom >= 17] { line-width: @pedestrian-width-z17; }
        [zoom >= 18] { line-width: @pedestrian-width-z18; }
        [zoom >= 19] { line-width: @pedestrian-width-z19; }
        #roads-casing {
          line-join: round;
          line-cap: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          line-join: round;
        }
      }
    }

    [feature = 'highway_living_street'] {
      [zoom >= 13] {
        line-width: @living-street-width-z13;
        [zoom >= 14] {
          line-width: @living-street-width-z14;
        }
        [zoom >= 15] { line-width: @living-street-width-z15; }
        [zoom >= 16] { line-width: @living-street-width-z16; }
        [zoom >= 17] { line-width: @living-street-width-z17; }
        [zoom >= 18] { line-width: @living-street-width-z18; }
        [zoom >= 19] { line-width: @living-street-width-z19; }
        #roads-casing {
          line-cap: round;
          line-join: round;
        }
        #tunnels {
          line-dasharray: 4,2;
        }
        #bridges {
          [zoom >= 14] {
            line-join: round;
          }
        }
      }
    }

    [feature = 'highway_steps'] {
      #bridges {
        [zoom >= 14][access != 'no'],
        [zoom >= 15] {
          line-width: @steps-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [zoom >= 15] { line-width: @steps-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          line-join: round;
        }
      }
      #tunnels {
        [zoom >= 13][access != 'no'],
        [zoom >= 15] {
          line-width: @steps-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          [zoom >= 15] { line-width: @steps-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          line-dasharray: 4,2;
        }
      }
    }

    [feature = 'highway_bridleway'],
    [feature = 'highway_path'][horse = 'designated'] {
      #bridges {
        [zoom >= 14][access != 'no'],
        [zoom >= 15] {
          line-width: @bridleway-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [zoom >= 15] { line-width: @bridleway-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          line-join: round;
        }
      }
      #tunnels {
        [zoom >= 13][access != 'no'],
        [zoom >= 15] {
          line-width: @bridleway-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          [zoom >= 15] { line-width: @bridleway-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          line-dasharray: 4,2;
        }
      }
    }

    [feature = 'highway_footway'],
    [feature = 'highway_path'][bicycle != 'designated'][horse != 'designated'] {
      #bridges {
        [zoom >= 14][access != 'no'],
        [zoom >= 15] {
          line-width: @footway-width-z14 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [zoom >= 15] { line-width: @footway-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 16] { line-width: @footway-width-z16 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 18] { line-width: @footway-width-z18 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 19] { line-width: @footway-width-z19 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          line-join: round;
        }
      }
      #tunnels {
        [zoom >= 14][access != 'no'],
        [zoom >= 15] {
          line-width: @footway-width-z14 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          [zoom >= 15] { line-width: @footway-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 16] { line-width: @footway-width-z16 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 18] { line-width: @footway-width-z18 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 19] { line-width: @footway-width-z19 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          line-dasharray: 4,2;
        }
      }
    }

    [feature = 'highway_cycleway'],
    [feature = 'highway_path'][bicycle = 'designated'] {
      #bridges {
        [zoom >= 14][access != 'no'],
        [zoom >= 15] {
          line-width: @cycleway-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [zoom >= 15] { line-width: @cycleway-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 16] { line-width: @cycleway-width-z16 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 18] { line-width: @cycleway-width-z18 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          [zoom >= 19] { line-width: @cycleway-width-z19 + 2 * (@paths-background-width + @paths-bridge-casing-width); }
          line-join: round;
        }
      }
      #tunnels {
        [zoom >= 13][access != 'no'],
        [zoom >= 15] {
          line-width: @cycleway-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          [zoom >= 15] { line-width: @cycleway-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 16] { line-width: @cycleway-width-z16 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 18] { line-width: @cycleway-width-z18 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          [zoom >= 19] { line-width: @cycleway-width-z19 + 2 * (@paths-background-width + @paths-tunnel-casing-width); }
          line-dasharray: 4,2;
        }
      }
    }

    [feature = 'highway_track'] {
      #bridges {
        [zoom >= 13][access != 'no'] {
          line-join: round;
          line-width: @track-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [tracktype = 'grade1'] {
            line-width: @track-grade1-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          }
          [tracktype = 'grade2'] {
            line-width: @track-grade2-width-z13 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          }
        }
        [zoom >= 15] {
          line-join: round;
          line-width: @track-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          [tracktype = 'grade1'] {
            line-width: @track-grade1-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          }
          [tracktype = 'grade2'] {
            line-width: @track-grade2-width-z15 + 2 * (@paths-background-width + @paths-bridge-casing-width);
          }
        }
      }
      #tunnels {
        [zoom >= 13][access != 'no'],
        [zoom >= 15] {
          line-dasharray: 4,2;
          line-width: @track-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          [tracktype = 'grade1'] {
            line-width: @track-grade1-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          }
          [tracktype = 'grade2'] {
            line-width: @track-grade2-width-z13 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
          }
          [zoom >= 15]{
            line-width: @track-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
            [tracktype = 'grade1'] {
              line-width: @track-grade1-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
            }
            [tracktype = 'grade2'] {
              line-width: @track-grade2-width-z15 + 2 * (@paths-background-width + @paths-tunnel-casing-width);
            }
          }
        }
      }
    }


    [feature = 'railway_tram'],
    [feature = 'railway_tram-service'][zoom >= 15] {
      #bridges {
        [zoom >= 13] {
          line-width: 4;
          [zoom >= 15] {
            line-width: 5;
          }
          line-join: round;
        }
      }
    }

    [feature = 'railway_subway'],
    [feature = 'railway_construction']['construction' = 'subway'] {
      #bridges {
        [zoom >= 14] {
          line-width: 5.5;
          line-join: round;
        }
      }
    }

    [feature = 'railway_light_rail'],
    [feature = 'railway_funicular'],
    [feature = 'railway_narrow_gauge'] {
      #bridges {
        [zoom >= 14] {
          line-width: 5.5;
          line-join: round;
        }
      }
    }

    [feature = 'railway_rail'],
    [feature = 'railway_preserved'],
    [feature = 'railway_monorail'][zoom >= 14] {
      #bridges {
        [zoom >= 13] {
          line-width: 6.5;
          line-join: round;
        }
      }
    }

    [feature = 'railway_INT-spur-siding-yard'] {
      #bridges {
        [zoom >= 13] {
          line-width: 5.7;
          line-join: round;
        }
      }
    }

    [feature = 'railway_disused'][zoom >= 15],
    [feature = 'railway_construction']['construction' != 'subway'],
    [feature = 'railway_miniature'][zoom >= 15],
    [feature = 'railway_INT-preserved-ssy'][zoom >= 14] {
      #bridges {
        [zoom >= 13] {
          line-width: 6;
          line-join: round;
        }
      }
    }

  }

  ::fill {
    line-color: @roads-fill-color;
    /*
     * The highway_construction rules below are quite sensitive to re-ordering, since the instances end up swapping round
     * (and then the dashes appear below the fills). See:
     * https://github.com/gravitystorm/openstreetmap-carto/issues/23
     * https://github.com/mapbox/carto/issues/235
     * https://github.com/mapbox/carto/issues/237
     */
    [feature = 'highway_construction'][zoom >= 12] {
      [construction = 'motorway'][zoom >= 12],
      [construction = 'motorway_link'][zoom >= 13],
      [construction = 'trunk'][zoom >= 12],
      [construction = 'trunk_link'][zoom >= 13],
      [construction = 'primary'][zoom >= 12],
      [construction = 'primary_link'][zoom >= 13],
      [construction = 'secondary'][zoom >= 12],
      [construction = 'secondary_link'][zoom >= 13],
      [construction = 'tertiary'][zoom >= 13],
      [construction = 'tertiary_link'][zoom >= 14] {
        line-width: 2;
        [zoom >= 13] {
          line-width: 4;
          b/line-width: 3.5;
          b/line-dasharray: 6,4;
        }
        [zoom >= 16] {
          line-width: 8;
          b/line-width: 7;
          b/line-dasharray: 8,6;
          [construction = 'secondary_link'] {
            line-width: @secondary-link-width-z15;
            b/line-width: @secondary-link-width-z15 - 2 * @casing-width-z15;
          }
          [construction = 'tertiary_link'] {
            line-width: @tertiary-link-width-z15;
            b/line-width: @tertiary-link-width-z15 - 2 * @casing-width-z15;
          }
        }
        [zoom >= 17] {
          line-width: 8;
          b/line-width: 7;
        }
      }

      [construction = null][zoom >= 14],
      [construction = 'residential'][zoom >= 14],
      [construction = 'unclassified'][zoom >= 14] {
        line-width: @residential-width-z14;
        [zoom >= 15] {
          line-width: @residential-width-z15;
          b/line-width: @residential-width-z15 - 2 * @casing-width-z15;
          }
        [zoom >= 16] {
          line-width: @residential-width-z16;
          b/line-width: @residential-width-z16 - 2 * @casing-width-z16;
          b/line-dasharray: 8,6;
        }
        [zoom >= 17] {
          line-width: 8;
          b/line-width: 7;
        }
      }
      [construction = 'living_street'][zoom >= 14] {
        line-width: @living-street-width-z14;
        [zoom >= 15] {
          line-width: @living-street-width-z15;
          b/line-width: @living-street-width-z15 - 2 * @casing-width-z15;
        }
        [zoom >= 16] {
          line-width: @living-street-width-z16;
          b/line-width: @living-street-width-z16 - 2 * @casing-width-z16;
          b/line-dasharray: 8,6;
        }
        [zoom >= 17] {
          line-width: 8;
          b/line-width: 7;
        }
      }
      [construction = 'pedestrian'][zoom >= 14] {
        line-width: @pedestrian-width-z14;
        [zoom >= 15] {
          line-width: @pedestrian-width-z15;
          b/line-width: @pedestrian-width-z15 - 2 * @casing-width-z15;
        }
        [zoom >= 16] {
          line-width: @pedestrian-width-z16;
          b/line-width: @pedestrian-width-z16 - 2 * @casing-width-z16;
          b/line-dasharray: 8,6;
        }
        [zoom >= 17] {
          line-width: 8;
          b/line-width: 7;
        }
      }

      [construction = 'service'] {
        [zoom >= 15][service = 'INT-normal'],
        [zoom >= 17][service = 'INT-minor'] {
          [service = 'INT-normal'] {
            line-width: @service-width-z14;
            b/line-width: @service-width-z14;
            [zoom >= 16] {
              line-width: @service-width-z16;
              b/line-width: @service-width-z16 - 2 * @casing-width-z16;
              b/line-dasharray: 8,6;
            }
            [zoom >= 17] {
              line-width: @service-width-z17;
              b/line-width: @service-width-z17 - 2 * @casing-width-z17;
            }
            [zoom >= 18] {
              line-width: 8;
              b/line-width: 7
            }
          }
          [service = 'INT-minor'] {
            line-width: @minor-service-width-z17;
            b/line-width: @minor-service-width-z17 - 2 * @casing-width-z17;
            b/line-dasharray: 8,6;
            [zoom >= 18] {
              line-width: @minor-service-width-z18;
              b/line-width: @minor-service-width-z18 - 2 * @casing-width-z18;
            }
            [zoom >= 19] {
              line-width: @minor-service-width-z19 - 2 * @casing-width-z19;
              b/line-width: @minor-service-width-z19 - 4 * @casing-width-z19;
            }
          }
        }
      }

      [construction = 'road'][zoom >= 15],
      [construction = 'raceway'][zoom >= 15] {
        line-width: @road-width-z14;
        [zoom >= 16] {
          line-width: @road-width-z16;
          b/line-width: @road-width-z16 - 2 * @casing-width-z16;
          b/line-dasharray: 8,6;
        }
        [zoom >= 17] {
          line-width: @road-width-z17;
          b/line-width: @road-width-z17 - 2 * @casing-width-z17;
        }
        [zoom >= 18] {
          line-width: 8;
          b/line-width: 7;
        }
      }
      [construction = 'footway'][zoom >= 15],
      [construction = 'cycleway'][zoom >= 15],
      [construction = 'bridleway'][zoom >= 15],
      [construction = 'path'][zoom >= 15],
      [construction = 'track'][zoom >= 15],
      [construction = 'steps'][zoom >= 15] {
        line-width: 3;
        line-opacity: 0.4;
      }
    }

    [feature = 'highway_motorway'] {
      [zoom >= 6][link != 'yes'],
      [zoom >= 10] {
        line-width: @motorway-width-z6;
        [zoom >= 7] { line-width: @motorway-width-z7; }
        [zoom >= 8] { line-width: @motorway-width-z8; }
        [zoom >= 9] { line-width: @motorway-width-z9; }
        [zoom >= 10] { line-width: @motorway-width-z10; }
        [zoom >= 11] { line-width: @motorway-width-z11; }
        [zoom >= 12] {
          line-width: @motorway-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @motorway-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @motorway-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @motorway-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @motorway-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @motorway-width-z19 - 2 * @major-casing-width-z19; }
          [zoom >= 20] { line-width: @motorway-width-z20 - 2 * @major-casing-width-z20; }
          [link = 'yes'] {
            line-width: @motorway-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @motorway-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @motorway-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @motorway-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @motorway-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @motorway-link-width-z19 - 2 * @casing-width-z19; }
            [zoom >= 20] { line-width: @motorway-link-width-z20 - 2 * @casing-width-z20; }
          }
          #bridges {
            line-width: @motorway-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @motorway-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @motorway-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @motorway-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @motorway-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @motorway-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @motorway-width-z20 - 2 * @major-bridge-casing-width-z20; }
            [link = 'yes'] {
              line-width: @motorway-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @motorway-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @motorway-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @motorway-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @motorway-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @motorway-link-width-z19 - 2 * @bridge-casing-width-z19; }
              [zoom >= 20] { line-width: @motorway-link-width-z20 - 2 * @bridge-casing-width-z20; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_trunk'] {
      [zoom >= 6][link != 'yes'],
      [zoom >= 10] {
        line-width: @trunk-width-z6;
        [zoom >= 7] { line-width: @trunk-width-z7; }
        [zoom >= 8] { line-width: @trunk-width-z8; }
        [zoom >= 9] { line-width: @trunk-width-z9; }
        [zoom >= 10] { line-width: @trunk-width-z10; }
        [zoom >= 11] { line-width: @trunk-width-z11; }
        [zoom >= 12] {
          line-width: @trunk-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @trunk-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @trunk-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @trunk-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @trunk-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @trunk-width-z19 - 2 * @major-casing-width-z19; }
          [link = 'yes'] {
            line-width: @trunk-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @trunk-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @trunk-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @trunk-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @trunk-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @trunk-link-width-z19 - 2 * @casing-width-z19; }
          }
          #bridges {
            line-width: @trunk-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @trunk-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @trunk-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @trunk-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @trunk-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @trunk-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [link = 'yes'] {
              line-width: @trunk-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @trunk-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @trunk-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @trunk-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @trunk-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @trunk-link-width-z19 - 2 * @bridge-casing-width-z19; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_primary'] {
      [zoom >= 8][link != 'yes'],
      [zoom >= 10] {
        line-width: @primary-width-z8;
        [zoom >= 9] { line-width: @primary-width-z9; }
        [zoom >= 10] { line-width: @primary-width-z10; }
        [zoom >= 11] { line-width: @primary-width-z11; }
        [zoom >= 12] {
          line-width: @primary-width-z12 - 2 * @major-casing-width-z12;
          [zoom >= 13] { line-width: @primary-width-z13 - 2 * @major-casing-width-z13; }
          [zoom >= 15] { line-width: @primary-width-z15 - 2 * @major-casing-width-z15; }
          [zoom >= 17] { line-width: @primary-width-z17 - 2 * @major-casing-width-z17; }
          [zoom >= 18] { line-width: @primary-width-z18 - 2 * @major-casing-width-z18; }
          [zoom >= 19] { line-width: @primary-width-z19 - 2 * @major-casing-width-z19; }
          [link = 'yes'] {
            line-width: @primary-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @primary-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @primary-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @primary-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @primary-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @primary-link-width-z19 - 2 * @casing-width-z19; }
          }
          #bridges {
            line-width: @primary-width-z12 - 2 * @major-bridge-casing-width-z12;
            [zoom >= 13] { line-width: @primary-width-z13 - 2 * @major-bridge-casing-width-z13; }
            [zoom >= 15] { line-width: @primary-width-z15 - 2 * @major-bridge-casing-width-z15; }
            [zoom >= 17] { line-width: @primary-width-z17 - 2 * @major-bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @primary-width-z18 - 2 * @major-bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @primary-width-z19 - 2 * @major-bridge-casing-width-z19; }
            [link = 'yes'] {
              line-width: @primary-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @primary-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @primary-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @primary-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @primary-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @primary-link-width-z19 - 2 * @bridge-casing-width-z19; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_secondary'] {
      [zoom >= 9][link != 'yes'],
      [zoom >= 10] {
        line-width: @secondary-width-z9;
        [zoom >= 10] { line-width: @secondary-width-z10; }
        [zoom >= 11] { line-width: @secondary-width-z11; }
        [zoom >= 12] {
          line-width: @secondary-width-z12 - 2 * @secondary-casing-width-z12;
          line-cap: round;
          line-join: round;
          [zoom >= 13] {
            [zoom >= 13] { line-width: @secondary-width-z13 - 2 * @secondary-casing-width-z13; }
            [zoom >= 14] { line-width: @secondary-width-z14 - 2 * @secondary-casing-width-z14; }
            [zoom >= 15] { line-width: @secondary-width-z15 - 2 * @secondary-casing-width-z15; }
            [zoom >= 16] { line-width: @secondary-width-z16 - 2 * @secondary-casing-width-z16; }
            [zoom >= 17] { line-width: @secondary-width-z17 - 2 * @secondary-casing-width-z17; }
            [zoom >= 18] { line-width: @secondary-width-z18 - 2 * @secondary-casing-width-z18; }
            [zoom >= 19] { line-width: @secondary-width-z19 - 2 * @secondary-casing-width-z19; }
            [link = 'yes'] {
              line-width: @secondary-link-width-z12 - 2 * @casing-width-z12;
              [zoom >= 13] { line-width: @secondary-link-width-z13 - 2 * @casing-width-z13; }
              [zoom >= 15] { line-width: @secondary-link-width-z15 - 2 * @casing-width-z15; }
              [zoom >= 17] { line-width: @secondary-link-width-z17 - 2 * @casing-width-z17; }
              [zoom >= 18] { line-width: @secondary-link-width-z18 - 2 * @casing-width-z18; }
              [zoom >= 19] { line-width: @secondary-link-width-z19 - 2 * @casing-width-z19; }
            }
            #bridges {
              line-width: @secondary-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @secondary-width-z13 - 2 * @major-bridge-casing-width-z13; }
              [zoom >= 14] { line-width: @secondary-width-z14 - 2 * @major-bridge-casing-width-z14; }
              [zoom >= 15] { line-width: @secondary-width-z15 - 2 * @major-bridge-casing-width-z15; }
              [zoom >= 16] { line-width: @secondary-width-z16 - 2 * @major-bridge-casing-width-z16; }
              [zoom >= 17] { line-width: @secondary-width-z17 - 2 * @major-bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @secondary-width-z18 - 2 * @major-bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @secondary-width-z19 - 2 * @major-bridge-casing-width-z19; }
              [link = 'yes'] {
                line-width: @secondary-link-width-z12 - 2 * @bridge-casing-width-z12;
                [zoom >= 13] { line-width: @secondary-link-width-z13 - 2 * @bridge-casing-width-z13; }
                [zoom >= 15] { line-width: @secondary-link-width-z15 - 2 * @bridge-casing-width-z15; }
                [zoom >= 17] { line-width: @secondary-link-width-z17 - 2 * @bridge-casing-width-z17; }
                [zoom >= 18] { line-width: @secondary-link-width-z18 - 2 * @bridge-casing-width-z18; }
                [zoom >= 19] { line-width: @secondary-link-width-z19 - 2 * @bridge-casing-width-z19; }
              }
            }
          }
        }
      }
    }

    [feature = 'highway_tertiary'] {
      [zoom >= 10] {
        line-width: @tertiary-width-z10;
        [zoom >= 11] {
          line-width: @tertiary-width-z11;
        }
        [zoom >= 12] {
          line-width: @tertiary-width-z12 - 2 * @casing-width-z12;
          [zoom >= 13] { line-width: @tertiary-width-z13 - 2 * @casing-width-z13; }
          [zoom >= 14] { line-width: @tertiary-width-z14 - 2 * @casing-width-z14; }
          [zoom >= 15] { line-width: @tertiary-width-z15 - 2 * @casing-width-z15; }
          [zoom >= 16] { line-width: @tertiary-width-z16 - 2 * @casing-width-z16; }
          [zoom >= 17] { line-width: @tertiary-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @tertiary-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @tertiary-width-z19 - 2 * @casing-width-z19; }
          [link = 'yes'] {
            line-width: @tertiary-link-width-z12 - 2 * @casing-width-z12;
            [zoom >= 13] { line-width: @tertiary-link-width-z13 - 2 * @casing-width-z13; }
            [zoom >= 15] { line-width: @tertiary-link-width-z15 - 2 * @casing-width-z15; }
            [zoom >= 17] { line-width: @tertiary-link-width-z17 - 2 * @casing-width-z17; }
            [zoom >= 18] { line-width: @tertiary-link-width-z18 - 2 * @casing-width-z18; }
            [zoom >= 19] { line-width: @tertiary-link-width-z19 - 2 * @casing-width-z19; }
          }
          #bridges {
            line-width: @tertiary-width-z12 - 2 * @bridge-casing-width-z12;
            [zoom >= 13] { line-width: @tertiary-width-z13 - 2 * @bridge-casing-width-z13; }
            [zoom >= 14] { line-width: @tertiary-width-z14 - 2 * @bridge-casing-width-z14; }
            [zoom >= 15] { line-width: @tertiary-width-z15 - 2 * @bridge-casing-width-z15; }
            [zoom >= 16] { line-width: @tertiary-width-z16 - 2 * @bridge-casing-width-z16; }
            [zoom >= 17] { line-width: @tertiary-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @tertiary-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @tertiary-width-z19 - 2 * @bridge-casing-width-z19; }
            [link = 'yes'] {
              line-width: @tertiary-link-width-z12 - 2 * @bridge-casing-width-z12;
              [zoom >= 13] { line-width: @tertiary-link-width-z13 - 2 * @bridge-casing-width-z13; }
              [zoom >= 15] { line-width: @tertiary-link-width-z15 - 2 * @bridge-casing-width-z15; }
              [zoom >= 17] { line-width: @tertiary-link-width-z17 - 2 * @bridge-casing-width-z17; }
              [zoom >= 18] { line-width: @tertiary-link-width-z18 - 2 * @bridge-casing-width-z18; }
              [zoom >= 19] { line-width: @tertiary-link-width-z19 - 2 * @bridge-casing-width-z19; }
            }
          }
          line-cap: round;
          line-join: round;
        }
      }
    }

    [feature = 'highway_residential'],
    [feature = 'highway_unclassified'] {
      [zoom = 12][feature = 'highway_residential'] {
        line-width: @residential-width-z12;
      }
      [zoom = 12][feature = 'highway_unclassified'] {
        line-width: @unclassified-width-z12;
      }
      [zoom >= 13] {
        line-width: @residential-width-z13 - 2 * @residential-casing-width-z13;
        [zoom >= 14] { line-width: @residential-width-z14 - 2 * @casing-width-z14; }
        [zoom >= 15] { line-width: @residential-width-z15 - 2 * @casing-width-z15; }
        [zoom >= 16] { line-width: @residential-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @residential-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @residential-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @residential-width-z19 - 2 * @casing-width-z19; }
        #bridges {
          line-width: @residential-width-z13 - 2 * @bridge-casing-width-z13;
          [zoom >= 14] { line-width: @residential-width-z14 - 2 * @bridge-casing-width-z14; }
          [zoom >= 15] { line-width: @residential-width-z15 - 2 * @bridge-casing-width-z15; }
          [zoom >= 16] { line-width: @residential-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @residential-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @residential-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @residential-width-z19 - 2 * @bridge-casing-width-z19; }
        }
        line-cap: round;
        line-join: round;
      }
    }

    [feature = 'highway_living_street'] {
      [zoom >= 13] {
        line-width: @living-street-width-z13 - 2 * @casing-width-z13;
        [zoom >= 14] { line-width: @living-street-width-z14 - 2 * @casing-width-z14; }
        [zoom >= 15] { line-width: @living-street-width-z15 - 2 * @casing-width-z15; }
        [zoom >= 16] { line-width: @living-street-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @living-street-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @living-street-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @living-street-width-z19 - 2 * @casing-width-z19; }
        #bridges {
          line-width: @living-street-width-z13 - 2 * @casing-width-z13;
          [zoom >= 14] { line-width: @living-street-width-z14 - 2 * @bridge-casing-width-z14; }
          [zoom >= 15] { line-width: @living-street-width-z15 - 2 * @bridge-casing-width-z15; }
          [zoom >= 16] { line-width: @living-street-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @living-street-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @living-street-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @living-street-width-z19 - 2 * @bridge-casing-width-z19; }
        }
        line-join: round;
        line-cap: round;
      }
    }

    [feature = 'highway_road'] {
      [zoom >= 10] {
        line-width: 1;
        line-join: round;
        line-cap: round;
      }
      [zoom >= 14] {
        line-width: @road-width-z14 - 2 * @casing-width-z14;
        [zoom >= 16] { line-width: @road-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @road-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @road-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @road-width-z19 - 2 * @casing-width-z19; }
        #bridges {
          line-width: @road-width-z14 - 2 * @bridge-casing-width-z14;
          [zoom >= 16] { line-width: @road-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @road-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @road-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @road-width-z19 - 2 * @bridge-casing-width-z19; }
        }
      }
    }

    [feature = 'highway_service'] {
      [zoom >= 14][service = 'INT-normal'],
      [zoom >= 16][service = 'INT-minor'] {
        [service = 'INT-normal'] {
          line-width: @service-width-z14 - 2 * @casing-width-z14;
          [zoom >= 16] { line-width: @service-width-z16 - 2 * @casing-width-z16; }
          [zoom >= 17] { line-width: @service-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @service-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @service-width-z19 - 2 * @casing-width-z19; }
          [zoom >= 20] { line-width: @service-width-z20 - 2 * @casing-width-z20; }
        }
        [service = 'INT-minor'] {
          line-width: @minor-service-width-z16 - 2 * @casing-width-z16;
          [zoom >= 17] { line-width: @minor-service-width-z17 - 2 * @casing-width-z17; }
          [zoom >= 18] { line-width: @minor-service-width-z18 - 2 * @casing-width-z18; }
          [zoom >= 19] { line-width: @minor-service-width-z19 - 2 * @casing-width-z19; }
          [zoom >= 20] { line-width: @minor-service-width-z20 - 2 * @casing-width-z20; }
        }
        line-join: round;
        line-cap: round;
        #bridges {
          [service = 'INT-normal'] {
            line-width: @service-width-z14 - 2 * @bridge-casing-width-z14;
            [zoom >= 16] { line-width: @service-width-z16 - 2 * @bridge-casing-width-z16; }
            [zoom >= 17] { line-width: @service-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @service-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @service-width-z19 - 2 * @bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @service-width-z20 - 2 * @bridge-casing-width-z20; }
          }
          [service = 'INT-minor'] {
            line-width: @minor-service-width-z16 - 2 * @bridge-casing-width-z16;
            [zoom >= 17] { line-width: @minor-service-width-z17 - 2 * @bridge-casing-width-z17; }
            [zoom >= 18] { line-width: @minor-service-width-z18 - 2 * @bridge-casing-width-z18; }
            [zoom >= 19] { line-width: @minor-service-width-z19 - 2 * @bridge-casing-width-z19; }
            [zoom >= 20] { line-width: @minor-service-width-z20 - 2 * @bridge-casing-width-z20; }
          }
        }
      }
    }

    [feature = 'highway_pedestrian'] {
      [zoom >= 14] {
        line-width: @pedestrian-width-z14 - 2 * @casing-width-z14;
        [zoom >= 15] { line-width: @pedestrian-width-z15 - 2 * @casing-width-z15; }
        [zoom >= 16] { line-width: @pedestrian-width-z16 - 2 * @casing-width-z16; }
        [zoom >= 17] { line-width: @pedestrian-width-z17 - 2 * @casing-width-z17; }
        [zoom >= 18] { line-width: @pedestrian-width-z18 - 2 * @casing-width-z18; }
        [zoom >= 19] { line-width: @pedestrian-width-z19 - 2 * @casing-width-z19; }
        #bridges {
          line-width: @pedestrian-width-z14 - 2 * @bridge-casing-width-z14;
          [zoom >= 15] { line-width: @pedestrian-width-z15 - 2 * @bridge-casing-width-z15; }
          [zoom >= 16] { line-width: @pedestrian-width-z16 - 2 * @bridge-casing-width-z16; }
          [zoom >= 17] { line-width: @pedestrian-width-z17 - 2 * @bridge-casing-width-z17; }
          [zoom >= 18] { line-width: @pedestrian-width-z18 - 2 * @bridge-casing-width-z18; }
          [zoom >= 19] { line-width: @pedestrian-width-z19 - 2 * @bridge-casing-width-z19; }
        }
        line-join: round;
        line-cap: round;
      }
    }

    [feature = 'highway_raceway'] {
      [zoom >= 12] {
        line-width: 1.2;
        line-join: round;
        line-cap: round;
      }
      [zoom >= 13] { line-width: 2; }
      [zoom >= 14] { line-width: 3; }
      [zoom >= 15] { line-width: 6; }
      [zoom >= 18] { line-width: 8; }
      [zoom >= 19] { line-width: 12; }
      [zoom >= 20] { line-width: 24; }
    }

    [feature = 'highway_platform'] {
      [zoom >= 16] {
        line-join: round;
        line-width: 6;
        line-cap: round;
      }
    }

    [feature = 'railway_rail'][zoom >= 8],
    [feature = 'railway_INT-spur-siding-yard'][zoom >= 13] {
      [zoom < 13] {
        line-width: 0.5;
        [zoom >= 8] { line-width: 0.8; }
        [zoom >= 12] { line-width: 0.9; }
        line-join: round;
        .roads_low_zoom[int_tunnel = 'yes'], #tunnels {
          line-dasharray: 5,2;
        }
      }
      [zoom >= 12] {
        #tunnels {
          line-width: 2.8;
          line-dasharray: 6,4;
          line-clip: false;
          [feature = 'railway_INT-spur-siding-yard'] {
            line-width: 1.9;
            line-dasharray: 3,3;
            [zoom >= 18] {
            line-width: 2.7;
            }
          }
          [feature = 'railway_rail'][zoom >= 18] {
            line-dasharray: 8,6;
            line-width: 3.8;
          }
        }
      }
    }

    [feature = 'railway_light_rail'],
    [feature = 'railway_funicular'],
    [feature = 'railway_narrow_gauge'] {
      [zoom >= 8] {
        line-width: 1;
        [zoom >= 13] { line-width: 2; }
        #tunnels {
          line-dasharray: 5,3;
        }
      }
    }

    [feature = 'railway_tram'],
    [feature = 'railway_tram-service'][zoom >= 15] {
      [zoom >= 12] {
        line-width: 0.75;
        [zoom >= 14] {
          line-width: 1;
        }
        [zoom >= 15] {
          line-width: 1.5;
          [feature = 'railway_tram-service'] {
            line-width: 0.5;
          }
        }
        [zoom >= 17] {
          line-width: 2;
          [feature = 'railway_tram-service'] {
            line-width: 1;
          }
        }
        [zoom >= 18] {
          [feature = 'railway_tram-service'] {
            line-width: 1.5;
          }
        }
        [zoom >= 19] {
          [feature = 'railway_tram-service'] {
            line-width: 2;
          }
        }
        #tunnels {
          line-dasharray: 5,3;
        }
      }
    }

    [feature = 'railway_subway'] {
      [zoom >= 12] {
        line-width: 2;
        #tunnels {
          line-dasharray: 5,3;
        }
      }
      #bridges {
        [zoom >= 14] {
          line-width: 2;
        }
      }
    }

    [feature = 'railway_construction'] {
      [zoom >= 13] {
        line-width: 2;
        line-dasharray: 2,4;
        line-join: round;
        [zoom >= 14] {
          line-dasharray: 2,3;
        }
        [zoom >= 15] {
          line-width: 3;
          line-dasharray: 3,3;
        }
      }
    }

    [feature = 'railway_disused'] {
      [zoom >= 15] {
        line-width: 2;
        line-dasharray: 2,4;
        line-join: round;
      }
    }

    [feature = 'railway_platform'] {
      [zoom >= 16] {
        line-join: round;
        line-width: 6;
        line-cap: round;
      }
    }

    [feature = 'railway_turntable'] {
      [zoom >= 16] {
        line-width: 1.5;
      }
    }
  }
}

#turning-circle-casing {
  [int_tc_type = 'tertiary'][zoom >= 15] {
    marker-fill: @roads-casing-color;
    marker-width: @tertiary-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @tertiary-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @tertiary-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @tertiary-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @tertiary-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @tertiary-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @tertiary-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @tertiary-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @tertiary-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @tertiary-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'residential'][zoom >= 15],
  [int_tc_type = 'unclassified'][zoom >= 15] {
    marker-fill: @roads-casing-color;
    marker-width: @residential-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @residential-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @residential-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @residential-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @residential-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @residential-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @residential-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @residential-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @residential-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @residential-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'living_street'][zoom >= 15] {
    marker-fill: @roads-casing-color;
    marker-width: @living-street-width-z15 * 1.6 + 2 * @casing-width-z15;
    marker-height: @living-street-width-z15 * 1.6 + 2 * @casing-width-z15;
    [zoom >= 16] {
      marker-width: @living-street-width-z16 * 1.6 + 2 * @casing-width-z16;
      marker-height: @living-street-width-z16 * 1.6 + 2 * @casing-width-z16;
    }
    [zoom >= 17] {
      marker-width: @living-street-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @living-street-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @living-street-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @living-street-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @living-street-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @living-street-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-normal'][zoom >= 16] {
    marker-fill: @roads-casing-color;
    marker-width: @service-width-z16 * 1.6 + 2 * @casing-width-z16;
    marker-height: @service-width-z16 * 1.6 + 2 * @casing-width-z16;
    [zoom >= 17] {
      marker-width: @service-width-z17 * 1.6 + 2 * @casing-width-z17;
      marker-height: @service-width-z17 * 1.6 + 2 * @casing-width-z17;
    }
    [zoom >= 18] {
      marker-width: @service-width-z18 * 1.6 + 2 * @casing-width-z18;
      marker-height: @service-width-z18 * 1.6 + 2 * @casing-width-z18;
    }
    [zoom >= 19] {
      marker-width: @service-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @service-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    [zoom >= 20] {
      marker-width: @service-width-z20 * 1.6 + 2 * @casing-width-z20;
      marker-height: @service-width-z20 * 1.6 + 2 * @casing-width-z20;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-minor'][zoom >= 18] {
    marker-fill: @roads-casing-color;
    marker-width: @minor-service-width-z18 * 1.6 + 2 * @casing-width-z18;
    marker-height: @minor-service-width-z18 * 1.6 + 2 * @casing-width-z18;
    [zoom >= 19] {
      marker-width: @minor-service-width-z19 * 1.6 + 2 * @casing-width-z19;
      marker-height: @minor-service-width-z19 * 1.6 + 2 * @casing-width-z19;
    }
    [zoom >= 20] {
      marker-width: @minor-service-width-z20 * 1.6 + 2 * @casing-width-z20;
      marker-height: @minor-service-width-z20 * 1.6 + 2 * @casing-width-z20;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }
}

#turning-circle-fill {
  [int_tc_type = 'tertiary'][zoom >= 15] {
    marker-fill: @roads-fill-color;
    marker-width: @tertiary-width-z15 * 1.6;
    marker-height: @tertiary-width-z15 * 1.6;
    [zoom >= 16] {
      marker-width: @tertiary-width-z16 * 1.6;
      marker-height: @tertiary-width-z16 * 1.6;
    }
    [zoom >= 17] {
      marker-width: @tertiary-width-z17 * 1.6;
      marker-height: @tertiary-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @tertiary-width-z18 * 1.6;
      marker-height: @tertiary-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @tertiary-width-z19 * 1.6;
      marker-height: @tertiary-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'residential'],
  [int_tc_type = 'unclassified'] {
    [zoom >= 15] {
      marker-fill: @roads-fill-color;
      marker-width: @residential-width-z15 * 1.6;
      marker-height: @residential-width-z15 * 1.6;
      [zoom >= 16] {
        marker-width: @residential-width-z16 * 1.6;
        marker-height: @residential-width-z16 * 1.6;
      }
      [zoom >= 17] {
        marker-width: @residential-width-z17 * 1.6;
        marker-height: @residential-width-z17 * 1.6;
      }
      [zoom >= 18] {
        marker-width: @residential-width-z18 * 1.6;
        marker-height: @residential-width-z18 * 1.6;
      }
      [zoom >= 19] {
        marker-width: @residential-width-z19 * 1.6;
        marker-height: @residential-width-z19 * 1.6;
      }
      marker-allow-overlap: true;
      marker-ignore-placement: true;
      marker-line-width: 0;
    }
  }

  [int_tc_type = 'living_street'][zoom >= 15] {
    marker-fill: @roads-fill-color;
    marker-width: @living-street-width-z15 * 1.6;
    marker-height: @living-street-width-z15 * 1.6;
    [zoom >= 16] {
      marker-width: @living-street-width-z16 * 1.6;
      marker-height: @living-street-width-z16 * 1.6;
    }
    [zoom >= 17] {
      marker-width: @living-street-width-z17 * 1.6;
      marker-height: @living-street-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @living-street-width-z18 * 1.6;
      marker-height: @living-street-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @living-street-width-z19 * 1.6;
      marker-height: @living-street-width-z19 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-normal'][zoom >= 16] {
    marker-fill: @roads-fill-color;
    marker-width: @service-width-z16 * 1.6;
    marker-height: @service-width-z16 * 1.6;
    [zoom >= 17] {
      marker-width: @service-width-z17 * 1.6;
      marker-height: @service-width-z17 * 1.6;
    }
    [zoom >= 18] {
      marker-width: @service-width-z18 * 1.6;
      marker-height: @service-width-z18 * 1.6;
    }
    [zoom >= 19] {
      marker-width: @service-width-z19 * 1.6;
      marker-height: @service-width-z19 * 1.6;
    }
    [zoom >= 20] {
      marker-width: @service-width-z20 * 1.6;
      marker-height: @service-width-z20 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'service'][int_tc_service = 'INT-minor'][zoom >= 18] {
    marker-fill: @roads-fill-color;
    marker-width: @minor-service-width-z18 * 1.6;
    marker-height: @minor-service-width-z18 * 1.6;
    [zoom >= 19] {
      marker-width: @minor-service-width-z19 * 1.6;
      marker-height: @minor-service-width-z19 * 1.6;
    }
    [zoom >= 20] {
      marker-width: @minor-service-width-z20 * 1.6;
      marker-height: @minor-service-width-z20 * 1.6;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }

  [int_tc_type = 'track'][zoom >= 15] {
    marker-fill: @roads-fill-color;
    marker-width: 6;
    marker-height: 6;
    [zoom >= 17] {
      marker-width: 10;
      marker-height: 10;
    }
    marker-allow-overlap: true;
    marker-ignore-placement: true;
    marker-line-width: 0;
  }
}

#junctions {
  [highway = 'motorway_junction'] {
    [zoom >= 11] {
      text-name: "[ref]";
      text-size: 10;
      text-fill: @roads-text-color;
      text-min-distance: 2;
      text-face-name: @oblique-fonts;
      text-halo-fill: @standard-halo-fill;
      text-halo-radius: @standard-halo-radius;
      text-wrap-character: ";";
      text-wrap-width: 2; // effectively break after every wrap character
      text-line-spacing: -1.5; // -0.15 em
      [zoom >= 13] {
        ["name" != null]["ref" = null] {
          text-name: "[name]";
        }
        ["name" != null]["ref" != null] {
          text-name: [name] + "\n" + [ref];
        }
      }
      [zoom >= 15] {
        text-size: 11;
        text-line-spacing: -1.65; // -0.15 em
      }
    }
  }

  [junction = 'yes'],
  [highway = 'traffic_signals'] {
    [zoom >= 15] {
      text-name: "[name]";
      text-size: 10;
      text-fill: @roads-text-color;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-wrap-width: 30;  // 3.0 em
      text-line-spacing: -1.5; // -0.15 em
      text-min-distance: 2;
      [zoom >= 17] {
        text-size: 11;
        text-line-spacing: -1.65; // -0.15 em
        /* Offset name on traffic_signals on zoomlevels where they are displayed
        in order not to hide the text */
        [highway = 'traffic_signals'] {
          text-dy: 9;
        }
      }
    }
  }
}

#bridge-text  {
  [man_made = 'bridge'] {
    [zoom >= 12][way_pixels > 62.5][way_pixels <= 64000] {
      text-name: "[name]";
      text-size: 10;
      text-wrap-width: 30; // 3 em
      text-line-spacing: -1.2; // -0.15 em
      text-fill: @roads-text-color;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-margin: 3; // 0.3 em
      text-wrap-width: 30;
      text-placement: interior;
      [way_pixels > 250] {
        text-size: 11;
        text-margin: 3.3; // 0.3 em
        text-wrap-width: 33; // 3 em
        text-line-spacing: -1.35; // -0.15 em
        text-halo-radius: @standard-halo-radius * 1.1;
      }
      [way_pixels > 1000] {
        text-size: 12;
        text-margin: 3.6; // 0.3 em
        text-wrap-width: 36; // 3 em
        text-line-spacing: -1.65; // -0.15 em
        text-halo-radius: @standard-halo-radius * 1.2;
      }
      [way_pixels > 4000] {
        text-size: 13;
        text-margin: 3.9; // 0.3 em
        text-wrap-width: 39; // 3 em
        text-line-spacing: -1.80; // -0.15 em
        text-halo-radius: @standard-halo-radius * 1.3;
      }
      [way_pixels > 16000] {
        text-size: 14;
        text-margin: 4.2; // 0.3 em
        text-wrap-width: 42; // 3 em
        text-line-spacing: -1.95; // -0.15 em
        text-halo-radius: @standard-halo-radius * 1.4;
      }
    }
  }
}

#guideways {
  [zoom >= 11][zoom < 13] {
    line-width: 0.6;
    line-color: @roads-fill-color;
    [zoom >= 12] { line-width: 1; }
  }
  [zoom >= 13] {
    line-width: 3;
    line-color: @roads-fill-color;
    line-join: round;
    b/line-width: 1;
    b/line-color: @roads-fill-color;
    b/line-dasharray: 8,12;
    b/line-join: round;
  }
  [zoom >= 14] {
    b/line-dasharray: 0,11,8,1;
  }
}

#aeroways {
  [aeroway = 'runway'] {
    [zoom >= 11] {
      ::casing[bridge = true][zoom >= 14] {
        line-width: 12 + 2*@major-casing-width-z14;
        line-color: @roads-casing-color;
        line-join: round;
        [zoom >= 15] { line-width: 18 + 2*@major-casing-width-z15; }
        [zoom >= 16] { line-width: 24 + 2*@major-casing-width-z16; }
        [zoom >= 17] { line-width: 24 + 2*@major-casing-width-z17; }
        [zoom >= 18] { line-width: 24 + 2*@major-casing-width-z18; }
      }
      ::fill {
        line-color: @roads-fill-color;
        line-width: 2;
        [zoom >= 12] { line-width: 4; }
        [zoom >= 13] { line-width: 6; }
        [zoom >= 14] { line-width: 12; }
        [zoom >= 15] { line-width: 18; }
        [zoom >= 16] { line-width: 24; }
      }
    }
  }
  [aeroway = 'taxiway'] {
    [zoom >= 11] {
      ::casing[bridge = true][zoom >= 14] {
        line-width: 4 + 2*@secondary-casing-width-z14;
        line-color: @roads-casing-color;
        line-join: round;
        [zoom >= 15] { line-width: 6 + 2*@secondary-casing-width-z15; }
        [zoom >= 16] { line-width: 8 + 2*@secondary-casing-width-z16; }
        [zoom >= 17] { line-width: 8 + 2*@secondary-casing-width-z17; }
        [zoom >= 18] { line-width: 8 + 2*@secondary-casing-width-z18; }
      }
      ::fill {
        line-color: @roads-fill-color;
        line-width: 1;
        [zoom >= 13] { line-width: 2; }
        [zoom >= 14] { line-width: 4; }
        [zoom >= 15] { line-width: 6; }
        [zoom >= 16] { line-width: 8; }
      }
    }
  }
}

#roads-text-name {
  [highway = 'motorway'],
  [highway = 'trunk'],
  [highway = 'primary'],
  [highway = 'construction'][construction = 'motorway'],
  [highway = 'construction'][construction = 'trunk'],
  [highway = 'construction'][construction = 'primary'] {
    [zoom >= 13] {
      text-name: "[name]";
      text-size: 8;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-repeat-distance: @major-highway-text-repeat-distance;
      [tunnel = 'no'] {
        text-halo-radius: @standard-halo-radius;
        [highway = 'motorway'] { text-halo-fill: @roads-halo-fill; }
        [highway = 'trunk'] { text-halo-fill: @roads-halo-fill; }
        [highway = 'primary'] { text-halo-fill: @roads-halo-fill; }
      }
    }
    [zoom >= 14] {
      text-size: 9;
    }
    [zoom >= 15] {
      text-size: 10;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'secondary'],
  [highway = 'construction'][construction = 'secondary'] {
    [zoom >= 13] {
      text-name: "[name]";
      text-size: 8;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 14] {
      text-size: 9;
    }
    [zoom >= 15] {
      text-size: 10;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'tertiary'],
  [highway = 'construction'][construction = 'tertiary'] {
    [zoom >= 14] {
      text-name: "[name]";
      text-size: 9;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
  [highway = 'construction'][construction = null][zoom >= 16] {
    text-name: "[name]";
    text-size: 9;
    text-fill: @roads-text-color;
    text-spacing: 300;
    text-clip: false;
    text-placement: line;
    text-halo-radius: @standard-halo-radius;
    text-halo-fill: @roads-halo-fill;
    text-face-name: @book-fonts;
    text-repeat-distance: @major-highway-text-repeat-distance;

    [zoom >= 17] {
      text-size: 11;
      text-spacing: 400;
    }
    [zoom >= 19] {
      text-size: 12;
      text-spacing: 400;
    }
  }
  [highway = 'residential'],
  [highway = 'unclassified'],
  [highway = 'road'],
  [highway = 'construction'][construction = null],
  [highway = 'construction'][construction = 'residential'],
  [highway = 'construction'][construction = 'unclassified'],
  [highway = 'construction'][construction = 'road'] {
    [zoom >= 15] {
      text-name: "[name]";
      text-size: 8;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-face-name: @book-fonts;
      text-repeat-distance: @minor-highway-text-repeat-distance;
      [highway = 'unclassified'] { text-repeat-distance: @major-highway-text-repeat-distance;}
    }
    [zoom >= 16] {
      text-size: 9;
    }
    [zoom >= 17] {
      text-size: 11;
      text-spacing: 400;
    }
    [zoom >= 19] {
      text-size: 12;
      text-spacing: 400;
    }
  }

  [highway = 'raceway'],
  [highway = 'service'],
  [highway = 'construction'][construction = 'raceway'],
  [highway = 'construction'][construction = 'service'][zoom >= 17] {
    [zoom >= 16] {
      text-name: "[name]";
      text-size: 9;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      [highway = 'raceway'] { text-halo-fill: @roads-halo-fill; }
      [highway = 'service'] { text-halo-fill: @roads-halo-fill; }
      text-face-name: @book-fonts;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 17] {
      text-size: 11;
    }
  }

  [highway = 'living_street'],
  [highway = 'pedestrian'],
  [highway = 'construction'][construction = 'living_street'][zoom >= 16],
  [highway = 'construction'][construction = 'pedestrian'][zoom >= 16] {
    [zoom >= 15] {
      text-name: "[name]";
      text-size: 8;
      text-fill: @roads-text-color;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-halo-radius: @standard-halo-radius;
      [highway = 'living_street'] {
        text-halo-fill: @roads-halo-fill;
        text-repeat-distance: @major-highway-text-repeat-distance;
      }
      [highway = 'pedestrian'] { text-halo-fill: @roads-halo-fill; }
      text-face-name: @book-fonts;
      text-repeat-distance: @minor-highway-text-repeat-distance;
    }
    [zoom >= 16] {
      text-size: 9;
    }
    [zoom >= 17] {
      text-size: 11;
    }
    [zoom >= 19] {
      text-size: 12;
    }
  }
}

#roads-area-text-name {
  [way_pixels > 3000],
  [zoom >= 17] {
    text-name: "[name]";
    text-fill: @roads-text-color;
    text-size: 11;
    text-face-name: @book-fonts;
    text-placement: interior;
    text-wrap-width: 30; // 2.7 em
    text-line-spacing: -1.7; // -0.15 em
  }
}

#paths-text-name {
  [highway = 'track'],
  [highway = 'construction'][construction = 'track'][zoom >= 16] {
    [zoom >= 15] {
      text-name: "[name]";
      text-fill: @roads-text-color;
      text-size: 8;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-vertical-alignment: middle;
      text-dy: 5;
      text-repeat-distance: @major-highway-text-repeat-distance;
    }
    [zoom >= 16] {
      text-size: 9;
      text-dy: 7;
    }
    [zoom >= 17] {
      text-size: 11;
      text-dy: 9;
    }
  }

  [highway = 'bridleway'],
  [highway = 'footway'],
  [highway = 'cycleway'],
  [highway = 'path'],
  [highway = 'steps'],
  [highway = 'construction'][construction = 'bridleway'],
  [highway = 'construction'][construction = 'footway'],
  [highway = 'construction'][construction = 'cycleway'],
  [highway = 'construction'][construction = 'path'],
  [highway = 'construction'][construction = 'steps'] {
    [zoom >= 16] {
      text-name: "[name]";
      text-fill: @roads-text-color;
      text-size: 9;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-spacing: 300;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-vertical-alignment: middle;
      text-dy: 7;
      text-repeat-distance: @major-highway-text-repeat-distance;
      [highway = 'steps'] { text-repeat-distance: @minor-highway-text-repeat-distance; }
    }
    [zoom >= 17] {
      text-size: 11;
      text-dy: 9;
    }
  }
}

#railways-text-name {
  /* Mostly started from z17. */
  [railway = 'rail'],
  [railway = 'subway'],
  [railway = 'narrow_gauge'],
  [railway = 'light_rail'],
  [railway = 'preserved'],
  [railway = 'funicular'],
  [railway = 'monorail'],
  [railway = 'tram'] {
    [zoom >= 17] {
      text-name: "[name]";
      text-fill: @roads-text-color;
      text-size: 10;
      text-dy: 6;
      text-spacing: 900;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-repeat-distance: @railway-text-repeat-distance;
    }
    [zoom >= 19] {
      text-size: 11;
      text-dy: 7;
    }
  }
  [railway = 'rail'] {
    /* Render highspeed rails from z11,
       other main routes at z14. */
    [highspeed = 'yes'] {
      [zoom >= 11] {
        text-name: "[name]";
        text-fill: @roads-text-color;
        text-size: 10;
        text-dy: 3;
        text-spacing: 300;
        text-clip: false;
        text-placement: line;
        text-face-name: @book-fonts;
        text-halo-radius: @standard-halo-radius;
        text-halo-fill: @roads-halo-fill;
        text-repeat-distance: @railway-text-repeat-distance;
      }
      [zoom >= 13] {
        text-dy: 6;
      }
      [zoom >= 14] {
        text-spacing: 600;
      }
      [zoom >= 17] {
        text-size: 11;
        text-dy: 7;
      }
      [zoom >= 19] {
        text-size: 12;
        text-dy: 8;
      }
    }
    [highspeed != 'yes'][usage = 'main'] {
      [zoom >= 14] {
        text-name: "[name]";
        text-fill: @roads-text-color;
        text-size: 10;
        text-dy: 6;
        text-spacing: 300;
        text-clip: false;
        text-placement: line;
        text-face-name: @book-fonts;
        text-halo-radius: @standard-halo-radius;
        text-halo-fill: @roads-halo-fill;
        text-repeat-distance: @railway-text-repeat-distance;
      }
      [zoom >= 17] {
        text-spacing: 600;
        text-size: 11;
        text-dy: 7;
      }
      [zoom >= 19] {
        text-size: 12;
        text-dy: 8;
      }
    }
  }
  /* Other minor railway styles. For service rails, see:
     https://github.com/gravitystorm/openstreetmap-carto/pull/2687 */
  [railway = 'preserved'],
  [railway = 'miniature'],
  [railway = 'disused'],
  [railway = 'construction'] {
    [zoom >= 17] {
      text-name: "[name]";
      text-fill: @roads-text-color;
      text-size: 10;
      text-dy: 6;
      text-spacing: 900;
      text-clip: false;
      text-placement: line;
      text-face-name: @book-fonts;
      text-halo-radius: @standard-halo-radius;
      text-halo-fill: @roads-halo-fill;
      text-repeat-distance: @railway-text-repeat-distance;
    }
    [zoom >= 19] {
      text-size: 11;
      text-dy: 7;
    }
  }
}
