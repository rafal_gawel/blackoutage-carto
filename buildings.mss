@building-fill: #1a1c1c;
@building-line: #0f0f0f;

#buildings {
  [zoom >= 14] {
    polygon-fill: @building-fill;
    polygon-clip: false;
    [zoom >= 15] {
      polygon-fill: @building-fill;
      line-color: @building-line;
      line-width: 1.5;
      line-clip: false;
    }
  }
}

#bridge {
  [zoom >= 12] {
    polygon-fill: @building-fill;
  }
}
