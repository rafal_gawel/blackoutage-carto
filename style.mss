Map {
  background-color: @land-color;
}

@water-color: #111A1D;
@land-color: #252828;
@text-color: #949494;
@roads-fill-color: #141414;
@roads-casing-color: #2C2E2D;

@standard-halo-radius: 2;
@standard-halo-fill: rgba(0,0,0,0.6);
